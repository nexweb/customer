package com.demo.microservices.customer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.microservices.customer.domain.Customer;
import com.demo.microservices.customer.repository.CustomerRepository;

@Service
public class CustomerService {
  @Autowired 
  CustomerRepository customerRepository; 

  public List<Customer> findByName(String customerName) {
    return customerRepository.findByName(customerName);
  }

  public List<Customer> findByAll() {
    return customerRepository.findAll();
  }


  public boolean add(Customer customer) {
    Customer cust = customerRepository.save(customer);
    return cust != null;
  }

  public boolean upsertCustomer(Customer customer) {
    Customer cust = customerRepository.findById(customer.getId()).get();
   
    if (cust != null) {
      cust = customerRepository.save(customer);
    }

    return cust != null;
  }

  public boolean deleteCustomer(Long customerId) {
    Customer customer = customerRepository.findById(customerId).get();

    if (customer != null)
      customerRepository.delete(customer);

    return customer != null;

  }
}

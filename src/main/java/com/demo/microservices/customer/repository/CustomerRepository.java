package com.demo.microservices.customer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.microservices.customer.domain.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
  public List<Customer> findByName(String name);
}

package com.demo.microservices.customer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.microservices.customer.domain.Customer;
import com.demo.microservices.customer.service.CustomerService;

import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CustomerController {

  @Autowired
  CustomerService customerService;
  
  @GetMapping("/")
  @ApiOperation(value = "Hello", notes = "Hello World")
  @ApiResponses({
    @ApiResponse(responseCode = "200", description = "OK !!") 
  })
  public Customer hello() {
    Customer cust = new Customer();
    cust.setId(0L);
    cust.setName("hello");
    cust.setNickname("hello");
    
    return cust;
  }

  @GetMapping("/customer/{customerName}")
  @ApiOperation(value = "Search Customer All", notes = "Search Customer All")
  @ApiResponses({
    @ApiResponse(responseCode = "200", description = "OK !!") 
  })
  public ResponseEntity<List<Customer>> findByCustomerName(@PathVariable String customerName) {
    log.info("customerName:{}", customerName);
    List<Customer> customerList = customerService.findByName(customerName);
    log.info("customerName:{}", customerList);
    return new ResponseEntity<List<Customer>>(customerList, HttpStatus.OK);
  }

  @GetMapping("/customer/")
  @ApiOperation(value = "Search Customer List by condition customerName", notes = "Search Customer List")
  public ResponseEntity<List<Customer>> findByAll() {
    List<Customer> customerList = customerService.findByAll();
    log.info("customerName:{}", customerList);
    return new ResponseEntity<List<Customer>>(customerList, HttpStatus.OK);
  }

  @PostMapping("/customer/")
  @ApiOperation(value = "Add Customer", notes = "Add Customer")
  public ResponseEntity<String> add(@RequestBody Customer customer ) {
    boolean ret = customerService.add(customer);
    log.info("Add customer:{}", ret);
    return new ResponseEntity<>("Added successful", HttpStatus.OK);
  }

  @PutMapping("/customer/")
  @ApiOperation(value = "Update Customer", notes = "Upsert CUstomer")
  public ResponseEntity<String> upSertCustomer(@RequestBody Customer customer ) {
    boolean ret = customerService.upsertCustomer(customer);
    log.info("Updated customer:{}", ret);
    return new ResponseEntity<>("Added successful", HttpStatus.OK);
  }

  @DeleteMapping("/customer/{customerId}")
  @ApiOperation(value = "Delete Customer", notes = "Delete Customer")
  public ResponseEntity<String> upSertCustomer(@PathVariable Long customerId ) {
    boolean ret = customerService.deleteCustomer(customerId);
    log.info("Deleted customer:{}", ret);
    return new ResponseEntity<>("Deleted successful", HttpStatus.OK);
  }
}

create table customer (id int primary key auto_increment, name varchar(30), nickname varchar(30))

insert into customer (name, nickname) values ('Nam HaMinh', 'nam@codejava.net')
insert into customer (name, nickname) values ('Ravi Kumar', 'ravi.kumar@gmail.com')